﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGWallet.Models.Entities
{
    public class Account
    {
        [Column("AccoutId")]
        public Guid Id { get; set; }
        public int AccountNumber { get; set; }
        [Column(TypeName = "decimal(38,2)")]
        public decimal Balance { get; set; }
        [ForeignKey("User")]
        public Guid UserId { get; set; }
    }
}
