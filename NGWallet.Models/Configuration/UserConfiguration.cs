﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NGWallet.Models.Entities;
using NGWallet.Models.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGWallet.Models.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasData
            (
                new User
                {
                    Id = new Guid("c9d4c053-49b6-410c-bc78-2d54a9991870"),
                    FirstName = "John",
                    LastName = "Doe",
                    Email = "johndoe@domain",
                    Gender = Gender.Male,
                },
                new User
                {
                    Id = new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3"),
                    FirstName = "Scarlett",
                    LastName = "Johannsen",
                    Email = "scarlettj@domain",
                    Gender = Gender.Female,
                }
            );
        }
    }
}
