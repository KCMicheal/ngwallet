﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NGWallet.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGWallet.Models.Configuration
{
    public class AccountConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.HasData
            (
                new Account
                {
                    Id = new Guid("80abbca8-664d-4b20-b5de-024705497d4a"),
                    AccountNumber = 1234567890,
                    Balance = 20000,
                    UserId = new Guid("c9d4c053-49b6-410c-bc78-2d54a9991870")
                },
                new Account
                {
                    Id = new Guid("80abbca8-664d-4b20-b5de-024705497d4a"),
                    AccountNumber = 1234567891,
                    Balance = 50000,
                    UserId = new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3")
                }
            );
        }
    }
}
